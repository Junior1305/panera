export const environment = {
  production: false,
  API_URL: 'http://localhost:3000',
  GENKEY_URL: 'http://localhost:3000',
  DEBUG: true,
  defaultLang: 'es',
};