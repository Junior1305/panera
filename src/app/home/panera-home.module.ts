import { NgModule } from '@angular/core';
import { HomeComponent } from './panera-home.component';
import { RouterModule } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const HOME_COMPONENTS = [
    HomeComponent,
  ];

@NgModule({
    declarations: HOME_COMPONENTS,
    imports: [
      RouterModule,
      NgbModule,
    ],
    exports: HOME_COMPONENTS,
    providers: [],
  })
  export class HomeModule {

  }