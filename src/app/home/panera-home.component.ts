import { Component, OnInit, Inject } from '@angular/core';
import { HomeService } from './panera-home.service';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panera-home',
  templateUrl: './panera-home.component.html',
  providers: [HomeService],
  styleUrls: ['./panera-home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, @Inject(LOCAL_STORAGE) private storage: WebStorageService) {      
  }
      
  ngOnInit(): void {
    if(this.storage.get('user-panera') != null) {
        
    } 
    else {
      //this.router.navigate(['login']);
    }
  }

  irCompras(){
    this.router.navigate(['compras']);
  }
  irInsumo(){
    this.router.navigate(['insumos']);
  }
  irInsumoalm(){
    this.router.navigate(['insumosalm']);
  }
  irAlmacenint(){
    this.router.navigate(['almacenint']);
  }
  irProducto(){
    this.router.navigate(['producto']);
  }
  irProductoalm(){
    this.router.navigate(['productoalm']);
  }
  irPedido(){
    this.router.navigate(['pedido']);
  }
  irDespacho(){
    this.router.navigate(['despacho']);
  }
  irDevolucion(){
    this.router.navigate(['devolucion']);
  }
  irAdmusu(){
    this.router.navigate(['admusu']);
  }
  irAdmper(){
    this.router.navigate(['admper']);
  }


  cerrarSesion() {
    this.storage.remove('user-panera');
    this.storage.remove('token-panera');
    this.router.navigate(['login']);
  }

}