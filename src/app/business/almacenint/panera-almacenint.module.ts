import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlmacenintBandejaComponent } from './bandeja/almacenint-bandeja.component';
import { AlmacenintRegistroComponent } from './registro/almacenint-registro.component';

const ALMACENINT_COMPONENTS = [ 
    AlmacenintBandejaComponent,
    AlmacenintRegistroComponent,
 ];

@NgModule({
    declarations: ALMACENINT_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: ALMACENINT_COMPONENTS
})

export class AlmacenintModule{

}