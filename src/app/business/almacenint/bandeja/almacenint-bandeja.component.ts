import { Component } from '@angular/core';
import { AlmacenintBandejaService } from './almacenint-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-almacenint-bandeja',
    templateUrl: './almacenint-bandeja.component.html',
    styleUrls: ['./almacenint-bandeja.component.scss'],
    providers: [ AlmacenintBandejaService ]
  })

export class AlmacenintBandejaComponent{

  constructor(private router: Router){

  }

  agregarAlmacenint(){
    this.router.navigate(['almacenint/registro']);
  }


}