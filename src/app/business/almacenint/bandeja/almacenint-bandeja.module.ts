import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlmacenintBandejaComponent } from './almacenint-bandeja.component';

@NgModule({
    declarations: [
        AlmacenintBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        AlmacenintBandejaComponent,
    ]
})

export class AlmacenintBandejaModule{

}