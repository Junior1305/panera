import { Component } from '@angular/core';
import { AlmacenintRegistroService } from './almacenint-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-almacenint-registro',
    templateUrl: './almacenint-registro.component.html',
    styleUrls: ['./almacenint-registro.component.scss'],
    providers: [ AlmacenintRegistroService ]
  })
export class AlmacenintRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['almacenint']);
  }

}