import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlmacenintRegistroComponent } from './almacenint-registro.component';

@NgModule({
    declarations: [
        AlmacenintRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        AlmacenintRegistroComponent,
    ]
})

export class AlmacenintRegistroModule{

}