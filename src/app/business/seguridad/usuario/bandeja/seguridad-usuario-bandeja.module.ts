import { NgModule } from '@angular/core';
import { SeguridadUsuarioBandejaComponent } from './seguridad-usuario-bandeja.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        SeguridadUsuarioBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        SeguridadUsuarioBandejaComponent,
    ]
})
export class SeguridadUsuarioBandejaModule{
    
}