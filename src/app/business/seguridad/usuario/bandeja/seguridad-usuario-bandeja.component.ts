import { Component } from '@angular/core';
import { SeguridadUsuarioBandejaService } from './seguridad-usuario-bandeja.service';

@Component({
    selector: 'app-seguridad-usuario-bandeja',
    templateUrl: './seguridad-usuario-bandeja.component.html',
    styleUrls: ['./seguridad-usuario-bandeja.component.scss'],
    providers: [ SeguridadUsuarioBandejaService ]
  })
export class SeguridadUsuarioBandejaComponent{
    
}