import { Component } from '@angular/core';
import { SeguridadUsuarioRegistroService } from './seguridad-usuario-registro.service';

@Component({
    selector: 'app-seguridad-usuario-registro',
    templateUrl: './seguridad-usuario-registro.component.html',
    styleUrls: ['./seguridad-usuario-registro.component.scss'],
    providers: [ SeguridadUsuarioRegistroService ]
  })
export class SeguridadUsuarioRegistroComponent{
    
}