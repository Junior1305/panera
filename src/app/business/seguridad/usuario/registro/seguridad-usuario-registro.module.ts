import { SeguridadUsuarioRegistroComponent } from './seguridad-usuario-registro.component';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        SeguridadUsuarioRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        SeguridadUsuarioRegistroComponent,
    ]
})
export class SeguridadUsuarioRegistroModule{
    
}