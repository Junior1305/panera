import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SeguridadPerfilBandejaComponent } from './perfil/bandeja/seguridad-perfil-bandeja.component';
import { SeguridadPerfilRegistroComponent } from './perfil/registro/seguridad-perfil-registro.component';
import { SeguridadUsuarioBandejaComponent } from './usuario/bandeja/seguridad-usuario-bandeja.component';
import { SeguridadUsuarioRegistroComponent } from './usuario/registro/seguridad-usuario-registro.component';

const SEGURIDAD_COMPONENTS = [ 
    SeguridadPerfilBandejaComponent,
    SeguridadPerfilRegistroComponent,
    SeguridadUsuarioBandejaComponent,
    SeguridadUsuarioRegistroComponent
 ];

@NgModule({
    declarations: SEGURIDAD_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: SEGURIDAD_COMPONENTS
})

export class SeguridadModule{

}