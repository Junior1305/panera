import { NgModule } from '@angular/core';
import { SeguridadPerfilRegistroComponent } from './seguridad-perfil-registro.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        SeguridadPerfilRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        SeguridadPerfilRegistroComponent,
    ]
})
export class SeguridadPerfilRegistroModule{
    
}