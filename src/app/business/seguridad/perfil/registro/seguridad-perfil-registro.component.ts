import { Component } from '@angular/core';
import { SeguridadPerfilRegistroService } from './seguridad-perfil-registro.service';

@Component({
    selector: 'app-seguridad-perfil-registro',
    templateUrl: './seguridad-perfil-registro.component.html',
    styleUrls: ['./seguridad-perfil-registro.component.scss'],
    providers: [ SeguridadPerfilRegistroService ]
  })
export class SeguridadPerfilRegistroComponent{
    
}