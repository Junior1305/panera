import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PerfilResponse } from '../models/perfil.response';
import { PaneraConstantes } from 'src/app/commons/util/panera-constantes';
import { PerfilRequest } from '../models/perfil.request';
import { PaneraResultResponse } from 'src/app/commons/model/panera-result.response';

@Injectable()
export class SeguridadPerfilBandejaService{

    constructor(private http: HttpClient){        
    }

    listarPerfiles() : Observable<PaneraResultResponse<PerfilResponse[]>>{            
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache').set('Ocp-Apim-Trace', 'true').set('Ocp-Apim-Subscription-Key', 'ea75addebe0a4b089355f7c20e8a06bc');
        return this.http.get<PaneraResultResponse<PerfilResponse[]>>(PaneraConstantes.API_SEGURIDAD_PERFIL_LISTAR, { headers : headers });        
    }

    obtenerPerfil() : Observable<PaneraResultResponse<PerfilResponse>>{            
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache').set('Ocp-Apim-Trace', 'true').set('Ocp-Apim-Subscription-Key', 'ea75addebe0a4b089355f7c20e8a06bc');
        return this.http.get<PaneraResultResponse<PerfilResponse>>(PaneraConstantes.API_SEGURIDAD_PERFIL_LISTAR, { headers : headers });        
    }

    registrarPerfil(perfilRequest : PerfilRequest) : Observable<PaneraResultResponse<PerfilResponse>>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache').set('Ocp-Apim-Trace', 'true').set('Ocp-Apim-Subscription-Key', 'ea75addebe0a4b089355f7c20e8a06bc');
        return this.http.post<PaneraResultResponse<PerfilResponse>>(PaneraConstantes.API_SEGURIDAD_PERFIL_REGISTRAR, perfilRequest, { headers : headers });   
    }

    actualizarPerfil(perfilRequest : PerfilRequest) : Observable<PaneraResultResponse<PerfilResponse>>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache').set('Ocp-Apim-Trace', 'true').set('Ocp-Apim-Subscription-Key', 'ea75addebe0a4b089355f7c20e8a06bc');
        return this.http.put<PaneraResultResponse<PerfilResponse>>(PaneraConstantes.API_SEGURIDAD_PERFIL_REGISTRAR, perfilRequest, { headers : headers });   
    }

}