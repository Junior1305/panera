import { NgModule } from '@angular/core';
import { SeguridadPerfilBandejaComponent } from './seguridad-perfil-bandeja.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        SeguridadPerfilBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        SeguridadPerfilBandejaComponent,
    ]
})

export class SeguridadPerfilBandejaModule{
    
}