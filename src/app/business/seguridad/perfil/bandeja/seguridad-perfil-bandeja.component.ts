import { Component } from '@angular/core';
import { SeguridadPerfilBandejaService } from './seguridad-perfil-bandeja.service';
import { Router } from '@angular/router';
import { PerfilRequest } from '../models/perfil.request';
import { PerfilResponse } from '../models/perfil.response';
import { Perfil } from '../models/perfil';

@Component({
    selector: 'app-seguridad-perfil-bandeja',
    templateUrl: './seguridad-perfil-bandeja.component.html',
    styleUrls: ['./seguridad-perfil-bandeja.component.scss'],
    providers: [ SeguridadPerfilBandejaService ]
  })
export class SeguridadPerfilBandejaComponent{
 
  public perfilRequest : PerfilRequest;
  public perfil : Perfil;
  public perfiles : PerfilResponse[];

  constructor(private router: Router, private perfilService : SeguridadPerfilBandejaService){

  }

  listarPerfiles(){    
    this.perfilService.listarPerfiles().subscribe(
      data => {
        if(data.success){
          this.perfiles = data.result;
        }
        else{
          console.log(data.message);
        }
      },
      err => { 
        console.log(err);
      }
    );
  }

  obtenerPerfiles(){    
    this.perfilService.obtenerPerfil().subscribe(
      data => {
        if(data.success){
          this.perfil = data.result;
        }
        else{
          console.log(data.message);
        }
      },
      err => { 
        console.log(err);
      }
    );
  }

  registrarPerfil(){
    this.perfilService.registrarPerfil(this.perfilRequest).subscribe(
      data => {
        if(data.success){

        }
        else{
          console.log(data.message);
        }        
      },
      err => { 
        console.log(err);
      }
    );
  }

  actualizarPerfil(){
    this.perfilService.actualizarPerfil(this.perfilRequest).subscribe(
      data => {
        if(data.success){

        }
        else{
          console.log(data.message);
        }        
      },
      err => { 
        console.log(err);
      }
    );
  }
  
  agregarPerfil(){
    this.router.navigate(['seguridad/perfil/registro']);
  }

  cancelar(){
    this.router.navigate(['seguridad/perfil']);
  }

}