import { Component } from '@angular/core';
import { DevolucionRegistroService } from './devolucion-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-devolucion-registro',
    templateUrl: './devolucion-registro.component.html',
    styleUrls: ['./devolucion-registro.component.scss'],
    providers: [ DevolucionRegistroService ]
  })
export class DevolucionRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['devolucion']);
  }

}