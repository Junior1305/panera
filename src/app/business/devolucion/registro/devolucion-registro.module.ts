import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DevolucionRegistroComponent } from './devolucion-registro.component';

@NgModule({
    declarations: [
        DevolucionRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        DevolucionRegistroComponent,
    ]
})

export class DevolucionRegistroModule{

}