import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DevolucionBandejaComponent } from './bandeja/devolucion-bandeja.component';
import { DevolucionRegistroComponent } from './registro/devolucion-registro.component';

const DEVOLUCION_COMPONENTS = [ 
    DevolucionBandejaComponent,
    DevolucionRegistroComponent,
 ];

@NgModule({
    declarations: DEVOLUCION_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: DEVOLUCION_COMPONENTS
})

export class DevolucionModule{

}