import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DevolucionBandejaComponent } from './devolucion-bandeja.component';

@NgModule({
    declarations: [
        DevolucionBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        DevolucionBandejaComponent,
    ]
})

export class DevolucionBandejaModule{

}