import { Component } from '@angular/core';
import { DevolucionBandejaService } from './devolucion-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-devolucion-bandeja',
    templateUrl: './devolucion-bandeja.component.html',
    styleUrls: ['./devolucion-bandeja.component.scss'],
    providers: [ DevolucionBandejaService ]
  })

export class DevolucionBandejaComponent{

  constructor(private router: Router){

  }

  agregarDevolucion(){
    this.router.navigate(['devolucion/registro']);
  }


}