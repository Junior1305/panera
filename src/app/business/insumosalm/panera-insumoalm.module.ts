import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InsumoalmBandejaComponent } from './bandeja/insumoalm-bandeja.component';
import { InsumoalmRegistroComponent } from './registro/insumoalm-registro.component';

const INSUMOALM_COMPONENTS = [ 
    InsumoalmBandejaComponent,
    InsumoalmRegistroComponent,
 ];

@NgModule({
    declarations: INSUMOALM_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: INSUMOALM_COMPONENTS
})

export class InsumoalmModule{

}