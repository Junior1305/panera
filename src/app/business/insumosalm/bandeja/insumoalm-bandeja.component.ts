import { Component } from '@angular/core';
import { InsumoalmBandejaService } from './insumoalm-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-insumoalm-bandeja',
    templateUrl: './insumoalm-bandeja.component.html',
    styleUrls: ['./insumoalm-bandeja.component.scss'],
    providers: [ InsumoalmBandejaService ]
  })

export class InsumoalmBandejaComponent{

  constructor(private router: Router){

  }

  agregarInsumoalm(){
    this.router.navigate(['insumosalm/registro']);
  }


}