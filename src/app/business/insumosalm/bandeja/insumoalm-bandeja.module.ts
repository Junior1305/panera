import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InsumoalmBandejaComponent } from './insumoalm-bandeja.component';

@NgModule({
    declarations: [
        InsumoalmBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        InsumoalmBandejaComponent,
    ]
})

export class InsumoalmBandejaModule{

}