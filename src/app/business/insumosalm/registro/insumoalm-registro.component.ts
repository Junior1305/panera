import { Component } from '@angular/core';
import { InsumoalmRegistroService } from './insumoalm-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-insumoalm-registro',
    templateUrl: './insumoalm-registro.component.html',
    styleUrls: ['./insumoalm-registro.component.scss'],
    providers: [ InsumoalmRegistroService ]
  })
export class InsumoalmRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['insumosalm']);
  }

}