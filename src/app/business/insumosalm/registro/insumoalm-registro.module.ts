import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InsumoalmRegistroComponent } from './insumoalm-registro.component';

@NgModule({
    declarations: [
        InsumoalmRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        InsumoalmRegistroComponent,
    ]
})

export class InsumoalmRegistroModule{

}