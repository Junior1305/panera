import { Component } from '@angular/core';
import { ProductoBandejaService } from './producto-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-producto-bandeja',
    templateUrl: './producto-bandeja.component.html',
    styleUrls: ['./producto-bandeja.component.scss'],
    providers: [ ProductoBandejaService ]
  })

export class ProductoBandejaComponent{

  constructor(private router: Router){

  }

  agregarProducto(){
    this.router.navigate(['producto/registro']);
  }


}