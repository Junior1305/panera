import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductoBandejaComponent } from './producto-bandeja.component';

@NgModule({
    declarations: [
        ProductoBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        ProductoBandejaComponent,
    ]
})

export class ProductoBandejaModule{

}