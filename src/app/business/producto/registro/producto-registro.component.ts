import { Component } from '@angular/core';
import { ProductoRegistroService } from './producto-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-producto-registro',
    templateUrl: './producto-registro.component.html',
    styleUrls: ['./producto-registro.component.scss'],
    providers: [ ProductoRegistroService ]
  })
export class ProductoRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['producto']);
  }

}