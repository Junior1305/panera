import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductoRegistroComponent } from './producto-registro.component';

@NgModule({
    declarations: [
        ProductoRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        ProductoRegistroComponent,
    ]
})

export class ProductoRegistroModule{

}