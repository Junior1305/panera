import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductoBandejaComponent } from './bandeja/producto-bandeja.component';
import { ProductoRegistroComponent } from './registro/producto-registro.component';

const PRODUCTO_COMPONENTS = [ 
    ProductoBandejaComponent,
    ProductoRegistroComponent,
 ];

@NgModule({
    declarations: PRODUCTO_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: PRODUCTO_COMPONENTS
})

export class ProductoModule{

}