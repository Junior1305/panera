import { Component } from '@angular/core';
import { AdmusuBandejaService } from './admusu-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-admusu-bandeja',
    templateUrl: './admusu-bandeja.component.html',
    styleUrls: ['./admusu-bandeja.component.scss'],
    providers: [ AdmusuBandejaService ]
  })

export class AdmusuBandejaComponent{

  constructor(private router: Router){

  }

  agregarAdmusu(){
    this.router.navigate(['admusu/registro']);
  }


}