import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdmusuBandejaComponent } from './admusu-bandeja.component';

@NgModule({
    declarations: [
        AdmusuBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        AdmusuBandejaComponent,
    ]
})

export class AdmusuBandejaModule{

}