import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdmusuRegistroComponent } from './admusu-registro.component';

@NgModule({
    declarations: [
        AdmusuRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        AdmusuRegistroComponent,
    ]
})

export class AdmusuRegistroModule{

}