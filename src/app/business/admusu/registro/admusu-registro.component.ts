import { Component } from '@angular/core';
import { AdmusuRegistroService } from './admusu-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-admusu-registro',
    templateUrl: './admusu-registro.component.html',
    styleUrls: ['./admusu-registro.component.scss'],
    providers: [ AdmusuRegistroService ]
  })
export class AdmusuRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['admusu']);
  }

}