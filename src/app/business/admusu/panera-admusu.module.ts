import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdmusuBandejaComponent } from './bandeja/admusu-bandeja.component';
import { AdmusuRegistroComponent } from './registro/admusu-registro.component';

const ADMUSU_COMPONENTS = [ 
    AdmusuBandejaComponent,
    AdmusuRegistroComponent,
 ];

@NgModule({
    declarations: ADMUSU_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: ADMUSU_COMPONENTS
})

export class AdmusuModule{

}