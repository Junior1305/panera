import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PedidoBandejaComponent } from './pedido-bandeja.component';

@NgModule({
    declarations: [
        PedidoBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        PedidoBandejaComponent,
    ]
})

export class PedidoBandejaModule{

}