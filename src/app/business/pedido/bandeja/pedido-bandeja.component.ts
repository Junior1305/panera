import { Component } from '@angular/core';
import { PedidoBandejaService } from './pedido-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-pedido-bandeja',
    templateUrl: './pedido-bandeja.component.html',
    styleUrls: ['./pedido-bandeja.component.scss'],
    providers: [ PedidoBandejaService ]
  })

export class PedidoBandejaComponent{

  constructor(private router: Router){

  }

  agregarPedido(){
    this.router.navigate(['pedido/registro']);
  }


}