import { Component } from '@angular/core';
import { PedidoRegistroService } from './pedido-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-pedido-registro',
    templateUrl: './pedido-registro.component.html',
    styleUrls: ['./pedido-registro.component.scss'],
    providers: [ PedidoRegistroService ]
  })
export class PedidoRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['pedido']);
  }

}