import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PedidoRegistroComponent } from './pedido-registro.component';

@NgModule({
    declarations: [
        PedidoRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        PedidoRegistroComponent,
    ]
})

export class PedidoRegistroModule{

}