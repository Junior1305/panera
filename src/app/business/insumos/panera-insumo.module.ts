import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InsumoBandejaComponent } from './bandeja/insumo-bandeja.component';
import { InsumoRegistroComponent } from './registro/insumo-registro.component';

const INSUMO_COMPONENTS = [ 
    InsumoBandejaComponent,
    InsumoRegistroComponent,
 ];

@NgModule({
    declarations: INSUMO_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: INSUMO_COMPONENTS
})

export class InsumoModule{

}