import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InsumoRegistroComponent } from './insumo-registro.component';

@NgModule({
    declarations: [
        InsumoRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        InsumoRegistroComponent,
    ]
})

export class InsumoRegistroModule{

}