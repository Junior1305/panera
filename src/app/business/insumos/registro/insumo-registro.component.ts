import { Component } from '@angular/core';
import { InsumoRegistroService } from './insumo-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-insumo-registro',
    templateUrl: './insumo-registro.component.html',
    styleUrls: ['./insumo-registro.component.scss'],
    providers: [ InsumoRegistroService ]
  })
export class InsumoRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['insumo']);
  }

}