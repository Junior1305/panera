import { Component } from '@angular/core';
import { InsumoBandejaService } from './insumo-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-insumo-bandeja',
    templateUrl: './insumo-bandeja.component.html',
    styleUrls: ['./insumo-bandeja.component.scss'],
    providers: [ InsumoBandejaService ]
  })

export class InsumoBandejaComponent{

  constructor(private router: Router){

  }

  agregarInsumo(){
    this.router.navigate(['insumos/registro']);
  }


}