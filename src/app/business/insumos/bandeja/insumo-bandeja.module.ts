import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InsumoBandejaComponent } from './insumo-bandeja.component';

@NgModule({
    declarations: [
        InsumoBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        InsumoBandejaComponent,
    ]
})

export class InsumoBandejaModule{

}