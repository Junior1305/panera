import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductoalmBandejaComponent } from './productoalm-bandeja.component';

@NgModule({
    declarations: [
        ProductoalmBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        ProductoalmBandejaComponent,
    ]
})

export class ProductoalmBandejaModule{

}