import { Component } from '@angular/core';
import { ProductoalmBandejaService } from './productoalm-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-productoalm-bandeja',
    templateUrl: './productoalm-bandeja.component.html',
    styleUrls: ['./productoalm-bandeja.component.scss'],
    providers: [ ProductoalmBandejaService ]
  })

export class ProductoalmBandejaComponent{

  constructor(private router: Router){

  }

  agregarProductoalm(){
    this.router.navigate(['productoalm/registro']);
  }


}