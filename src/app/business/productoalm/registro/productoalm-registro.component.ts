import { Component } from '@angular/core';
import { ProductoalmRegistroService } from './productoalm-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-productoalm-registro',
    templateUrl: './productoalm-registro.component.html',
    styleUrls: ['./productoalm-registro.component.scss'],
    providers: [ ProductoalmRegistroService ]
  })
export class ProductoalmRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['productoalm']);
  }

}