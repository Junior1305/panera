import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductoalmRegistroComponent } from './productoalm-registro.component';

@NgModule({
    declarations: [
        ProductoalmRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        ProductoalmRegistroComponent,
    ]
})

export class ProductoalmRegistroModule{

}