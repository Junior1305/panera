import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductoalmBandejaComponent } from './bandeja/productoalm-bandeja.component';
import { ProductoalmRegistroComponent } from './registro/productoalm-registro.component';

const PRODUCTOALM_COMPONENTS = [ 
    ProductoalmBandejaComponent,
    ProductoalmRegistroComponent,
 ];

@NgModule({
    declarations: PRODUCTOALM_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: PRODUCTOALM_COMPONENTS
})

export class ProductoalmModule{

}