import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComprasBandejaComponent } from './bandeja/compras-bandeja.component';
import { ComprasRegistroComponent } from './registro/compras-registro.component';

const COMPRAS_COMPONENTS = [ 
    ComprasBandejaComponent,
    ComprasRegistroComponent,
 ];

@NgModule({
    declarations: COMPRAS_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: COMPRAS_COMPONENTS
})

export class ComprasModule{

}