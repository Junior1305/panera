import { Component } from '@angular/core';
import { ComprasBandejaService } from './compras-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-compras-bandeja',
    templateUrl: './compras-bandeja.component.html',
    styleUrls: ['./compras-bandeja.component.scss'],
    providers: [ ComprasBandejaService ]
  })

export class ComprasBandejaComponent{

  constructor(private router: Router){

  }

  agregarCompra(){
    this.router.navigate(['compras/registro']);
  }


}