import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComprasBandejaComponent } from './compras-bandeja.component';

@NgModule({
    declarations: [
        ComprasBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        ComprasBandejaComponent,
    ]
})

export class ComprasBandejaModule{

}