import { Component } from '@angular/core';
import { ComprasRegistroService } from './compras-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-compras-registro',
    templateUrl: './compras-registro.component.html',
    styleUrls: ['./compras-registro.component.scss'],
    providers: [ ComprasRegistroService ]
  })
export class ComprasRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['compras']);
  }

}