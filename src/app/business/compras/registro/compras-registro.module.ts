import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComprasRegistroComponent } from './compras-registro.component';

@NgModule({
    declarations: [
        ComprasRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        ComprasRegistroComponent,
    ]
})

export class ComprasRegistroModule{

}