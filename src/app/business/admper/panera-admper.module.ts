import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdmperBandejaComponent } from './bandeja/admper-bandeja.component';
import { AdmperRegistroComponent } from './registro/admper-registro.component';

const ADMPER_COMPONENTS = [ 
    AdmperBandejaComponent,
    AdmperRegistroComponent,
 ];

@NgModule({
    declarations: ADMPER_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: ADMPER_COMPONENTS
})

export class AdmperModule{

}