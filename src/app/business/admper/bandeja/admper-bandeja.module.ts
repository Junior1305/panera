import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdmperBandejaComponent } from './admper-bandeja.component';

@NgModule({
    declarations: [
        AdmperBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        AdmperBandejaComponent,
    ]
})

export class AdmperBandejaModule{

}