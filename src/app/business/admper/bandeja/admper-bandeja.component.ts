import { Component } from '@angular/core';
import { AdmperBandejaService } from './admper-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-admper-bandeja',
    templateUrl: './admper-bandeja.component.html',
    styleUrls: ['./admper-bandeja.component.scss'],
    providers: [ AdmperBandejaService ]
  })

export class AdmperBandejaComponent{

  constructor(private router: Router){

  }

  agregarAdmper(){
    this.router.navigate(['admper/registro']);
  }


}