import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdmperRegistroComponent } from './admper-registro.component';

@NgModule({
    declarations: [
        AdmperRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        AdmperRegistroComponent,
    ]
})

export class AdmperRegistroModule{

}