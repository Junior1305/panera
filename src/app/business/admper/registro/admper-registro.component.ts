import { Component } from '@angular/core';
import { AdmperRegistroService } from './admper-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-admper-registro',
    templateUrl: './admper-registro.component.html',
    styleUrls: ['./admper-registro.component.scss'],
    providers: [ AdmperRegistroService ]
  })
export class AdmperRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['admper']);
  }

}