import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DespachoBandejaComponent } from './bandeja/despacho-bandeja.component';
import { DespachoRegistroComponent } from './registro/despacho-registro.component';

const DESPACHO_COMPONENTS = [ 
    DespachoBandejaComponent,
    DespachoRegistroComponent,
 ];

@NgModule({
    declarations: DESPACHO_COMPONENTS,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: DESPACHO_COMPONENTS
})

export class DespachoModule{

}