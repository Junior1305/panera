import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DespachoRegistroComponent } from './despacho-registro.component';

@NgModule({
    declarations: [
        DespachoRegistroComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        DespachoRegistroComponent,
    ]
})

export class DespachoRegistroModule{

}