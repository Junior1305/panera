import { Component } from '@angular/core';
import { DespachoRegistroService } from './despacho-registro.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-despacho-registro',
    templateUrl: './despacho-registro.component.html',
    styleUrls: ['./despacho-registro.component.scss'],
    providers: [ DespachoRegistroService ]
  })
export class DespachoRegistroComponent{

  constructor(private router: Router){

  }
  
  cancelar(){
    this.router.navigate(['despacho']);
  }

}