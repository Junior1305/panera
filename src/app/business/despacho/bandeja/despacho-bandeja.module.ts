import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DespachoBandejaComponent } from './despacho-bandeja.component';

@NgModule({
    declarations: [
        DespachoBandejaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        DespachoBandejaComponent,
    ]
})

export class DespachoBandejaModule{

}