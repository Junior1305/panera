import { Component } from '@angular/core';
import { DespachoBandejaService } from './despacho-bandeja.services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-despacho-bandeja',
    templateUrl: './despacho-bandeja.component.html',
    styleUrls: ['./despacho-bandeja.component.scss'],
    providers: [ DespachoBandejaService ]
  })

export class DespachoBandejaComponent{

  constructor(private router: Router){

  }

  agregarDespacho(){
    this.router.navigate(['despacho/registro']);
  }


}