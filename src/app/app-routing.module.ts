import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/panera-login.component';
import { HomeComponent } from './home/panera-home.component';
import { ComprasBandejaComponent } from './business/compras/bandeja/compras-bandeja.component';
import { ComprasRegistroComponent } from './business/compras/registro/compras-registro.component';
import { InsumoBandejaComponent } from './business/insumos/bandeja/insumo-bandeja.component';
import { InsumoRegistroComponent } from './business/insumos/registro/insumo-registro.component';
import { InsumoalmBandejaComponent } from './business/insumosalm/bandeja/insumoalm-bandeja.component';
import { InsumoalmRegistroComponent } from './business/insumosalm/registro/insumoalm-registro.component';
import { AlmacenintBandejaComponent } from './business/almacenint/bandeja/almacenint-bandeja.component';
import { AlmacenintRegistroComponent } from './business/almacenint/registro/almacenint-registro.component';
import { ProductoBandejaComponent } from './business/producto/bandeja/producto-bandeja.component';
import { ProductoRegistroComponent } from './business/producto/registro/producto-registro.component';
import { ProductoalmBandejaComponent } from './business/productoalm/bandeja/productoalm-bandeja.component';
import { ProductoalmRegistroComponent } from './business/productoalm/registro/productoalm-registro.component';
import { PedidoBandejaComponent } from './business/pedido/bandeja/pedido-bandeja.component';
import { PedidoRegistroComponent } from './business/pedido/registro/pedido-registro.component';
import { DespachoBandejaComponent } from './business/despacho/bandeja/despacho-bandeja.component';
import { DespachoRegistroComponent } from './business/despacho/registro/despacho-registro.component';
import { DevolucionBandejaComponent } from './business/devolucion/bandeja/devolucion-bandeja.component';
import { DevolucionRegistroComponent } from './business/devolucion/registro/devolucion-registro.component';
import { AdmusuBandejaComponent } from './business/admusu/bandeja/admusu-bandeja.component';
import { AdmusuRegistroComponent } from './business/admusu/registro/admusu-registro.component';
import { AdmperBandejaComponent } from './business/admper/bandeja/admper-bandeja.component';
import { AdmperRegistroComponent } from './business/admper/registro/admper-registro.component';
import { SeguridadUsuarioBandejaComponent } from './business/seguridad/usuario/bandeja/seguridad-usuario-bandeja.component';
import { SeguridadUsuarioRegistroComponent } from './business/seguridad/usuario/registro/seguridad-usuario-registro.component';
import { SeguridadPerfilBandejaComponent } from './business/seguridad/perfil/bandeja/seguridad-perfil-bandeja.component';
import { SeguridadPerfilRegistroComponent } from './business/seguridad/perfil/registro/seguridad-perfil-registro.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'seguridad/usuario', component: SeguridadUsuarioBandejaComponent },
  { path: 'seguridad/usuario/registro', component: SeguridadUsuarioRegistroComponent },
  { path: 'seguridad/perfil', component: SeguridadPerfilBandejaComponent },
  { path: 'seguridad/perfil/registro', component: SeguridadPerfilRegistroComponent },
  { path: 'compras', component: ComprasBandejaComponent },
  { path: 'compras/registro', component: ComprasRegistroComponent },
  { path: 'insumos', component: InsumoBandejaComponent },
  { path: 'insumos/registro', component: InsumoRegistroComponent },
  { path: 'insumosalm', component: InsumoalmBandejaComponent },
  { path: 'insumosalm/registro', component: InsumoalmRegistroComponent },
  { path: 'almacenint', component: AlmacenintBandejaComponent },
  { path: 'almacenint/registro', component: AlmacenintRegistroComponent },
  { path: 'producto', component: ProductoBandejaComponent },
  { path: 'producto/registro', component: ProductoRegistroComponent },
  { path: 'productoalm', component: ProductoalmBandejaComponent },
  { path: 'productoalm/registro', component: ProductoalmRegistroComponent },
  { path: 'pedido', component: PedidoBandejaComponent },
  { path: 'pedido/registro', component: PedidoRegistroComponent },
  { path: 'despacho', component: DespachoBandejaComponent },
  { path: 'despacho/registro', component: DespachoRegistroComponent },
  { path: 'devolucion', component: DevolucionBandejaComponent },
  { path: 'devolucion/registro', component: DevolucionRegistroComponent },
  { path: 'admusu', component: AdmusuBandejaComponent },
  { path: 'admusu/registro', component: AdmusuRegistroComponent },
  { path: 'admper', component: AdmperBandejaComponent },
  { path: 'admper/registro', component: AdmperRegistroComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
