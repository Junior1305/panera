import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StorageServiceModule} from 'angular-webstorage-service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/panera-login.module';
import { HomeModule } from './home/panera-home.module';
import { ComprasModule } from './business/compras/panera-compras.module';
import { InsumoModule } from './business/insumos/panera-insumo.module';
import { InsumoalmModule } from './business/insumosalm/panera-insumoalm.module';
import { AlmacenintModule } from './business/almacenint/panera-almacenint.module';
import { ProductoModule } from './business/producto/panera-producto.module';
import { ProductoalmModule } from './business/productoalm/panera-productoalm.module';
import { PedidoModule } from './business/pedido/panera-pedido.module';
import { DespachoModule } from './business/despacho/panera-despacho.module';
import { DevolucionModule } from './business/devolucion/panera-devolucion.module';
import { AdmusuModule } from './business/admusu/panera-admusu.module';
import { AdmperModule } from './business/admper/panera-admper.module';
import { SeguridadModule } from './business/seguridad/panera-seguridad.module';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule, 
    AppRoutingModule,
    StorageServiceModule,
    ComprasModule,
    InsumoModule,
    InsumoalmModule,
    AlmacenintModule,
    ProductoModule,
    ProductoalmModule,
    PedidoModule,
    DespachoModule,
    DevolucionModule,
    AdmusuModule,
    AdmperModule,
    SeguridadModule,
    LoginModule,
    HomeModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }