
import {HttpClient, HttpHeaders, HttpResponse, HttpEvent, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PaneraLoginResponse } from './model/panera-login.response';
import { PaneraResultResponse } from '../commons/model/panera-result.response';
import { PaneraConstantes } from '../commons/util/panera-constantes';

@Injectable({
    providedIn: 'root'
})
export class PaneraLoginService {

    constructor(private http: HttpClient){

    }

    private handleError(errorResponse:HttpErrorResponse){
        if(errorResponse.error instanceof ErrorEvent){
            console.error('Client side error:   ', errorResponse.error.message);
        }else{
            console.error('Server side error', errorResponse);
        }
        return throwError('Aqui hay un problema con servicio');
    }

    // getLogin(paneraLoginRequest : PaneraLoginReq uest) : Observable<PaneraResultResponse<PaneraLoginResponse>>{
    //     let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache').set('Ocp-Apim-Trace', 'true').set('Ocp-Apim-Subscription-Key', 'ea75addebe0a4b089355f7c20e8a06bc');
    //     return this.http.get<PaneraResultResponse<PaneraLoginResponse>>(PaneraConstantes.API_SEGURIDAD_PERFIL_LOGIN,{headers : headers});
    // }
    
    getLogin(usuario:string,contrasena:string): Observable<PaneraResultResponse<PaneraLoginResponse>>{
        var request : any  = { "usuario" : usuario , "contrasena" : contrasena };
        return this.http.post<PaneraResultResponse<PaneraLoginResponse>>(PaneraConstantes.API_SEGURIDAD_PERFIL_LOGIN,request,{
            headers: new HttpHeaders({
                'Content-Type':'application/json'
            })
        })
        .pipe(catchError(this.handleError));
    }
}