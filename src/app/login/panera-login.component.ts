import { Component, OnInit, Inject } from '@angular/core';
import { PaneraLoginService } from './panera-login.service';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';


@Component({
  selector: 'panera-login',
  templateUrl: './panera-login.component.html',
  providers: [PaneraLoginService],
  styleUrls: ['./panera-login.component.scss']
})


export class LoginComponent implements OnInit {
  private _success = new Subject<string>();

  successMessage: string;
  public respuesta;
  public procesando;
  constructor(private router: Router, private loginService: PaneraLoginService) { }

  ngOnInit() {}

  //
  public loginUsuario(form: NgForm) {
    this.loginService.getLogin(form.value.usuario, form.value.contrasena).subscribe(
      response => {
        if (response.success) {
          this.router.navigate(['home']);
        } else {
          alert(response.message);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
