import { NgModule } from '@angular/core';
import { LoginComponent } from './panera-login.component';
import { HttpClientModule } from '@angular/common/http';
import { PaneraLoginService } from './panera-login.service';
import { FormsModule } from '@angular/forms';

const LOGIN_COMPONENTS = [
    LoginComponent,
  ];

@NgModule({
    declarations: LOGIN_COMPONENTS,
    imports: [
      HttpClientModule,
      FormsModule
    ],
    exports: LOGIN_COMPONENTS,
    providers: [PaneraLoginService],
  })
  export class LoginModule {

  }